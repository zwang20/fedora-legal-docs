////

Copyright Fedora Project Authors.

SPDX-License-Identifier: CC-BY-SA-4.0

////

= License audit tooling for Fedora packages

This page describes the tools used in standard practice to audit licensing of packages in the Fedora Linux distribution.

== Packaging tools

These tools are used for evaluating the state of licensing in Fedora Linux packages. These use the https://gitlab.com/fedora/legal/fedora-license-data[Fedora License Data] as a source for valid licenses.

=== RPMLint

https://github.com/rpm-software-management/rpmlint[RPMLint] is the standard tool used for evaluating Fedora Linux packages for well-known issues for packagers to fix.
In the context of licensing, RPMLint evaluates the `License:` field in the spec file and ensures the values comply with xref:allowed-licenses.adoc[the known standard of allowed licenses].

This is packaged in Fedora Linux as `rpmlint`.

=== RPMInspect

https://github.com/rpminspect/rpminspect[RPMInspect] is the tool used to evaluate Fedora Linux packages for policy compliance as they are built in the Fedora Build System.
In the context of licensing, RPMInspect evaluates the `License:` field in RPMs and ensures the values comply with xref:allowed-licenses.adoc[the known standard of allowed licenses].

This is packaged in Fedora Linux as `rpminspect`. To use it, you need both `rpminspect` and `rpminspect-data-fedora`.


== License and source inspection tools

These tools are used for evaluating the state of licensing in the software being packaged for Fedora Linux. All of these tools are distribution-agnostic.

=== Licensecheck

https://metacpan.org/dist/App-Licensecheck[Licensecheck] is a tool used to evaluate source files for their licensing. This tool is principally used in the Fedora context for
the initial package review for packages to be included in the Fedora Linux distribution. It is run automaically as part of https://pagure.io/FedoraReview[FedoraReview].

Licensecheck, by default, provides license reports with the full license names, but can be told to produce output using any number of license identifier schemes.

This is packaged in Fedora Linux as `licensecheck`.

=== SPDX-license-diff
https://github.com/spdx/spdx-license-diff[SPDX-license-diff] is a Firefox and Chromium/Chrome plugin that compares highlighted text of a license as displayed on a web page to find the closest match to something on the SPDX License List. If a match to an SPDX identifier is given as close to but less than 100%, SPDX-license-diff will show you differences in the texts. 

*Good for:* Quick identification as compared to SPDX License List for a specific license text.

*Tips for using:*
In some cases, differences will display that are actually accounted for by the SPDX matching guidelines. If it's a close match, (e.g., only the name of the copyright holder or author, or other non-substantive seeming differences) it is recommended to cross-check against the https://github.com/spdx/license-list-XML/tree/main/src[actual template] that the SPDX license list uses to implement (some of) the matching guidelines. 

=== SPDX Check License
https://tools.spdx.org/app/check_license/[SPDX Check License] (https://github.com/spdx/spdx-online-tools[source code])-  is a Django application in which you paste the text of a license or exception into a text box and it will match it against all the licenses and exceptions in the SPDX License List, implementing the SPDX Matching Guidelines. Because of its thoroughness, this tool may take more time to give an answer than SPDX-license-diff. It will tell you if there is a match or not and if it finds a close match, but won't indicate a diff.

=== askalano

*How to find or use:*

*Good for:* Useful for quick analysis of packages coming out of
ecosystems featuring projects known to have (1) highly standardized
approaches to layout of license information (e.g., (it
specifically looks only for files that are named LICENSE or COPYING or
some obvious variant on those), (2) generally simple
license makeup, and (3) cultural preferences for a highly limited set
of licenses (for example, Rust crates that don't bundle legacy C code,
Golang modules, Node.js npm packages).

*Shortcomings:* It can't recognize or understand: (1) license notices/license texts that are comments in source files, (2) license notices/license texts in README files, (3) license files that contain multiple license texts (or it will only
recognize the first of them), (4) nonstandard/archaic/legacy licenses (which covers most of the
licenses being reviewed in issues in fedora-license-data)

*Packaged in Fedora Linux:* ??

=== FOSSology
https://www.fossology.org/[FOSSology] is a license compliance software system and tooklit that includes license scanning. The information here focuses on that aspect of the toolkit. It can be run locally and also can be set up as a hosted services. See https://www.fossology.org/get-started/[Get Started] for ways to install and a link to a test instance that anyone can use.

*Good for:* Scanning an entire package for licenses or text that looks like licenses. Ability to view files easily in interface and remember license inspection decisions. 

*Tips on using:* 
* In options: #5 - check "Ignore SCM files"; #7 - check Monk, Nomos, Ojo License Analysis and Package Analysis; #8 - check first two options re: "Scanners matches..." 
* Go to License Browswer view. Look for license matches that are suspicious or unexpected, such as things that are not an SPDX id or ambiguous. You can then view the files with those matches and inspect what was found to determine if there is a license that needs to be recorded or if it is a false match. https://www.fossology.org/get-started/basic-workflow/[Basic Workflow] has some helpful information.

Packaged in Fedora Linux: No

=== ScanCode toolkit
https://github.com/nexB/scancode-toolkit[ScanCode] is a comprehensive tool, written in Python, for detecting licenses and related information in source code. ScanCode output reports detected license information using both ScanCode's own non-SPDX system of license keys and corresponding SPDX expressions; however it is not clear that ScanCode uses the SPDX matching guidelines in making such determinations. 
