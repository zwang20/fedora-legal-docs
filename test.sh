#!/usr/bin/bash
podman run --rm -it -v "$(pwd):/antora:z" "docker.io/antora/antora" --html-url-extension-style=indexify site.yml
cd public
python3 -m http.server 8080

